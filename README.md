# Keybase KeyProvider [![Build Status](https://gitlab.com/nodepass/keybase-key-provider/badges/master/build.svg)](https://gitlab.com/nodepass/keybase-key-provider/pipelines) [![Known Vulnerabilities](https://snyk.io/test/npm/keybase-key-provider/badge.svg)](https://snyk.io/test/npm/keybase-key-provider) [![codecov](https://codecov.io/gl/nodepass/keybase-key-provider/branch/master/graph/badge.svg)](https://codecov.io/gl/nodepass/keybase-key-provider)


A KeyProvider for [Node Password Store](https://gitlab.com/nodepass/node-password-store)
that will fetch a private key from [Keybase](https://keybase.io).

> This is a separate module because the Keybase API is currently in
> [alpha](https://keybase.io/docs/api/1.0/intro).

## Installation

```
npm install keybase-key-provider
```

## Usage

### Initialization
```js
const KeybaseKeyProvider = require("keybase-key-provider")

/* Pass the user's Keybase credentials into the KeyProvider constructor. */
const keyProvider = new KeybaseKeyProvider(email_or_username, passphrase)
```

### Use with Node Password Store
```js
/* This can be passed into the Node Password Store constructor. */
const Pass = require("password-store")
const pass = new Pass(keyProvider.keyFetcher)
```

### Use with kbpgp.js
```js
/*
 * Like other KeyProviders, a `keyFetcher` property is present.
 * This property is a promise that resolves with a `kbpgp.KeyFetcher` object.
 * We can use this with `kbpgp` to encrypt/decrypt data.
 */
const kbpgp = require("kbpgp")
keyProvider.keyFetcher.then(function(keyFetcher) {
	kbpgp.box(
		{encrypt_for: keyFetcher, msg: "Secret Squirrel"},
		function(err, result) {
			console.log(err, result)
		}
	)
})
```

## License
Keybase KeyProvider is licensed under the [BSD 2-Clause License](./LICENSE).
