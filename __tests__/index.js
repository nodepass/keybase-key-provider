jest.mock("keybase-login")

const KeybaseKeyProvider = require("../src")

describe("keybase-key-provider", function() {
	it("should return a KeyFetcher for a valid username and passphrase", function() {
		let keyProvider = new KeybaseKeyProvider("johndoe", "secret")
		return keyProvider.keyFetcher
			.then((keyFetcher) => {
				expect(keyFetcher).toBeDefined()
			})
			.catch(() => fail("failed to create a KeyFetcher"))
	})

	it("should return a KeyFetcher for a valid email and passphrase", function() {
		let keyProvider = new KeybaseKeyProvider("test@example.com", "secret")
		return keyProvider.keyFetcher
			.then((keyFetcher) => {
				expect(keyFetcher).toBeDefined()
			})
			.catch(() => fail("failed to create a KeyFetcher"))
	})

	it("should throw an error if an invalid login is provided", function() {
		let keyProvider = new KeybaseKeyProvider("foo", "bar")
		return keyProvider.keyFetcher
			.then(() => fail("no error was thrown"))
			.catch((e) => expect(e).toBeDefined())
	})

	it("should throw an error if no login is provided", function() {
		let keyProvider = new KeybaseKeyProvider()
		return keyProvider.keyFetcher
			.then(() => fail("no error was thrown"))
			.catch((e) => expect(e).toBeDefined())
	})
})
