const KeyManager = require("kbpgp").KeyManager
const login = require("keybase-login").login

class KeybaseKeyProvider {
	constructor(email_or_username, passphrase) {
		this.keyFetcher = new Promise((resolve, reject) => {
			const auth = { passphrase }
			if (email_or_username.indexOf("@") > -1) {
				auth.email = email_or_username
			} else {
				auth.username = email_or_username
			}
			login(auth, (err, res) => {
				if (err) return reject(err)
				resolve(res)
			})
		})
		.then((res) => res.me.private_keys.primary.bundle)
		.then((armored) => {
			return new Promise((resolve, reject) => {
				KeyManager.import_from_p3skb({ armored }, (err, km) => {
					if (err) return reject(err)
					if (km.is_p3skb_locked()) {
						km.unlock_p3skb({ passphrase }, (err) => {
							if (err) return reject(err)
							resolve(km)
						})
					} else {
						resolve(km)
					}
				})
			})
		})
	}
}

module.exports = KeybaseKeyProvider
