const KeybaseLogin = require.requireActual("keybase-login")

const users = [{
	username: "johndoe",
	email: "test@example.com",
	passphrase: "secret",
	key: require("../__tests__/lib/keys/p3skb-key")
}]

KeybaseLogin.login = function(opts, cb) {
	if (!opts.username && !opts.email) {
		cb(new Error("username or email is required"))
	}
	else if (!opts.passphrase) {
		cb(new Error("passphrase is required"))
	}
	else {
		let err, res
		for (let user of users) {
			if ((!!opts.username && user.username === opts.username) || (!!opts.email && user.email === opts.email)) {
				if (user.passphrase === opts.passphrase) {
					res = {
						me: {
							private_keys: {
								primary: {
									bundle: user.key
								}
							}
						}
					}
					break
				}
				else {
					err = new Error("bad passphase")
					break
				}
			}
		}
		if (!res && !err) err = new Error("unkown user")
		cb(err, res)
	}
}

module.exports = KeybaseLogin
