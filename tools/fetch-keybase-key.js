/* eslint-disable no-console */
const prompt = require("prompt")

const PgpUtils = require("./utils/pgp")
const KeybaseKeyProvider = require("..")

const schema = {
	properties: {
		email_or_username: {
			required: true
		},
		passphrase: {
			hidden: true,
			required: true
		}
	}
}

function debug(a) {
	console.log(a)
	return a
}

prompt.start()
prompt.get(schema, function(err, res) {
	if (err) {
		console.error(err)
		process.exit(1)
	}

	let key
	const kp = new KeybaseKeyProvider(res.email_or_username, res.passphrase)
	kp.keyFetcher
		.then((kf) => {key = kf; return key})
		.then(() => PgpUtils.encrypt("secretsquirrel\n", key))
		.then(debug)
		.then((msg) => PgpUtils.decryptToString(msg, key))
		.then(debug)
		.catch(console.error)
})
