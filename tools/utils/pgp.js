const kbpgp = require("kbpgp")

function decrypt(msg, keyFetcher, asp) {
	return new Promise(function(resolve, reject) {
		kbpgp.unbox({
			keyfetch: keyFetcher,
			armored: msg,
			asp
		}, function(err, literals) {
			if (err) return reject(err)
			resolve(literals)
		})
	})
}

function decryptToString(msg, keyFetcher, asp) {
	return decrypt(msg, keyFetcher, asp)
		.then((literals) => literals.map((l) => l.toString()))
		.then((msgs) => msgs.join())
}

function encrypt(msg, keyFetcher, asp) {
	return new Promise(function(resolve, reject) {
		kbpgp.box({
			msg,
			encrypt_for: keyFetcher,
			asp
		}, function(err, result) {
			if (err) return reject(err)
			resolve(result)
		})
	})
}

module.exports = {
	decrypt,
	decryptToString,
	encrypt
}
