# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

## 1.0.0-alpha.2 - 2016-12-07
### Added
- Add unit tests

### Changed
- Update README

### Fixed
- Add workaround for Codecov detection of GitLab CI

## 1.0.0-alpha.1 - 2016-12-01
- Initial published version
